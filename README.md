# Digitas mock test

Digitas Mock test in Drupal 8 CMS.

Composer version 1.10.3

Drupal Version 8.9.3

PHP Version 7.2.25

Database Version 5.5.5-10.4.10-MariaDB

We have created a custom module(bank_cheque_generator) to create user form using Form API.

We have created a custom theme and use the base theme as drupal default "classy" theme.

We have created a view page for list all users data.

---------------FOR SETUP-------------------

STEP 1: git clone https://gitlab.com/astikkumar.patel/digitas-mock-test.git

SETP 2: open web/sites/default/settings.local.php file and update below credentials:

		$databases['default']['default'] = array (
			  'database' => 'DB NAME',
			  'username' => 'USERNAME',
			  'password' => 'PASSWORD',
			  'prefix' => '',
			  'host' => 'HOST',
			  'port' => '3306',
			  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
			  'driver' => 'mysql',
			);

STEP 2: Setup Drupal in the system and create DB

STEP 3: Select 'Digitas' in Appearance->Digitas theme 'Install and set as default'.

STEP 4: Create a new content type and machine name should have to 'user_data'
			In CMS Structure->content type follow below steps:

			1. Add new content type
			2. Add 4 fields and all fields machine name should have (field_first_name, field_last_name, field_payee_name, field_sum)

STEP 5: Install custom Module 'Bank Cheque Generator'

STEP 6: URL: [site name]/userdata URL for user form

We have developed 2 ways to store data in DB. We have added both functionalities in custom form.

	1. create a custom content type('user_data') to store records.
	2. create a custom table and store data in the table. For that, We need to create a table in DB.

We are using content type('user_data') for develope and display bank check generator functionality.

<b>Note: We need to create new custom type and machine name should have to 'user_data' for working bank cheque generator functionality.</b>