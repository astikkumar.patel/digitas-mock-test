<?php
/**
 * @file
 * Contains \Drupal\bank_cheque_generator\Form\ChequeForm.
 */
namespace Drupal\bank_cheque_generator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

class ChequeForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bank_cheque_generator_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name:'),
      '#required' => TRUE,
    
    );

    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name:'),
      '#required' => TRUE,
      
    );
    $form['payee_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Payee Name:'),
      '#required' => TRUE,
   );

    $form['sum'] = array (
      '#type' => 'textfield',
      '#title' => t('The Sum'),
      '#required' => TRUE,
      '#maxlength' => 15,
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Generate Cheque'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {
         
        $amount = $form_state->getValue('sum');
        if (is_numeric($form_state->getValue('first_name')) ) {
             $form_state->setErrorByName('first_name', t('Please add string data.'));
        }
        if(is_numeric($form_state->getValue('payee_name')))
        {
           $form_state->setErrorByName('payee_name', t('Please add string data.'));
        }
        if(is_numeric($form_state->getValue('last_name'))  )
        {
           $form_state->setErrorByName('last_name', t('Please add string data.'));
        }
        if (strlen($amount) > 15) {
        $form_state->setErrorByName('sum', $this->t('Amount is too much high.'));
        }
        if (!is_numeric($amount) || strpos($amount, '.') != '' ) {
             $form_state->setErrorByName('sum', t('Please add Amount in Integer format.'));
        } 
         
    }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
   
  /* 1. custom type to store data */  
   
  $name =  $form_state->getValue('first_name') .' '. $form_state->getValue('last_name');
  $node = Node::create([
    'type'        => 'user_data',
    'title'       => $name,
    'field_first_name'       => $form_state->getValue('first_name'),
    'field_last_name'       => $form_state->getValue('last_name'),
    'field_payee_name'       => $form_state->getValue('payee_name'),
    'field_sum'       => $form_state->getValue('sum'),
  ]);
  
  $node->save();
  $node->id();

  /* 2. custom table create in DB to store data */  
  /*
   $conn = Database::getConnection();
    $conn->insert('users_bank_data')->fields(
      array(
        'first_name' => $form_state->getValue('first_name'),
        'last_name' => $form_state->getValue('last_name'),
        'payee_name' => $form_state->getValue('payee_name'),
        'amount' => $form_state->getValue('sum'),
      )
    )->execute(); 
  */
  $form_state->setRedirect(
        'entity.node.canonical',
        ['node' => $node->id()]
      );

   }
}